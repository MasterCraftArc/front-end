
import React, { useEffect, useState } from 'react';


function ConferenceForm() {

    const getData = async () => {
        const url = "http://localhost:8000/api/locations/";

        const response = await fetch (url)
        if(response.ok){
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        getData();
      }, []);

    const [name, setName] = useState('')
    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const [starts, setStart] = useState('')
    const handleStartChange = (event) => {
        const value = event.target.value
        setStart(value)
    }
    const [ends, setEnd] = useState ('')
    const handleEndsChange = (event) => {
        const value = event.target.value
        setEnd(value)
    }
    const [description, setDescription] = useState ('')
    const handleDescriptionChange = (event) => {
        const value = event.target.value
        setDescription(value)
    }
    const [presentations, setPresentations] = useState ('')
    const handlePresentations = (event) => {
        const value = event.target.value
        setPresentations(value)
    }
    const [attendees, setAttendees] = useState ('')
    const handleattendees = (event) => {
        const value = event.target.value
        setAttendees(value)
    }

    const [locations, setLocations] = useState ([])

    const [location, setLocation] = useState("")
    const handlelocations = (event) => {
        const value = event.target.value
        setLocation(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.name = name
        data.starts = starts
        data.ends = ends
        data.description = description
        data.max_presentations = presentations
        data.max_attendees = attendees
        data.location = location

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers:{
                'Content-Type':'application/json',
            }

        }

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);
          setName('');
          setStart('');
          setEnd('');
          setDescription('');
          setPresentations('');
          setAttendees('');
          setLocation('');
        }
    }
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} name="name" placeholder="Name" required type="text" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange}name="starts" placeholder="starts" required type="date" id="starts" className="form-control" />
                <label htmlFor="starts">starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange}name="ends" placeholder="ends" required type="date" id="ends" className="form-control" />
                <label htmlFor="ends">ends</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={handleDescriptionChange}name="description" id="description" placeholder="Description" rows="6"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresentations}name="max_presentations" placeholder="max_presentations" required type="number" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleattendees}name="max_attendees" placeholder="max_attendees" required type="text" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handlelocations}name="location" required id="location" className="form-select" defaultValue="">
                  <option value="">
                    Choose a location</option>
                    {locations.map(location => {
            return (
            <option key={location.id}value={location.id}>
            {location.name}
            </option>
    );
  })}

                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
