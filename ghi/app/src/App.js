import AttendeesList from "./AttendeesList"
import Nav from "./Nav";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendeesForm from "./AttendeesForm"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";
function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
        <Nav />
      <div className="container">
        <Routes>
        <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="attendees">
            <Route path="list" element={<AttendeesList attendees={props.attendees}/>} />
            <Route path="new" element={<AttendeesForm />} />
          </Route>
          <Route path="conferences">
            <Route path="list" element={<AttendeesList />} />
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
