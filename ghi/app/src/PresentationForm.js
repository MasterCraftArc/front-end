import React, {useEffect, useState } from 'react';

function PresentationForm() {


  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [company, setCompany] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [conferences, setConferences] = useState([]);
  const [conference, setConference] = useState('');

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleConference = (event) => {
    const value = event.target.value
    setConference(value)
}

const handleName = (event) => {
  const value = event.target.value
  setName(value)
}

const handleEmail = (event) => {
  const value = event.target.value
  setEmail(value)
}

const handleCompany = (event) => {
const value = event.target.value
setCompany(value)
}
const handleTitle = (event) => {
  const value = event.target.value
  setTitle(value)
}

const handleSynopsis = (event) => {
const value = event.target.value
setSynopsis(value)
}

const handleSubmit = async (event) => {
  event.preventDefault()

  const data = {}
  data.presenter_name = name
  data.presenter_email = email
  data.company_name = company
  data.title = title
  data.synopsis = synopsis


  const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`
  const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers:{
          'Content-Type':'application/json',
      }

  }
  const response = await fetch(presentationUrl, fetchConfig);
  if (response.ok) {
    const newLocation = await response.json();
    console.log(newLocation);

    setName('');
    setEmail('');
    setCompany('');
    setTitle('');
    setSynopsis('');
    setConference('');
  }
}



  return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                  <div className="form-floating mb-3">
                    <input onChange={handleName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                    <label htmlFor="presenter_name">Presenter name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                    <label htmlFor="presenter_email">Presenter email</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleCompany} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                    <label htmlFor="company_name">Company name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                    <label htmlFor="title">Title</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea onChange={handleSynopsis} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
                  </div>
                  <div className="mb-3">
                    <select onChange={handleConference} required name="conference" id="conference" className="form-select" value={conference}>
                      <option value="">Choose a conference</option>

                      {conferences?.map(conference => {
            return (
            <option key={conference.href} value={conference.id}>
            {conference.name}
            </option>
            );
          })}

                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>

  );
}



export default PresentationForm
